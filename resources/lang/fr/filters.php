<?php

return [
    'both'=>'Tous les enregistrements',
    'translated'=>'Entièrement traduit',
    'not_translated'=>'Traduction manquante'
];