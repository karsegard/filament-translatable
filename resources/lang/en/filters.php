<?php

return [
    'both'=>'All records',
    'translated'=>'Only translated records',
    'not_translated'=>'Only untranslated records'
];