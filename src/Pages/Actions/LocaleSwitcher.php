<?php

namespace KDA\Filament\Translatable\Pages\Actions;

use Filament\Facades\SpatieLaravelTranslatablePlugin;
use Filament\Pages\Actions\SelectAction;

class LocaleSwitcher extends SelectAction
{
    public static function getDefaultName(): ?string
    {
        return 'activeLocale';
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->label('Langue');
        $this->options(function (): array {
            $livewire = $this->getLivewire();
            
            if (! method_exists($livewire, 'getTranslatableLocales')) {
                return [];
            }

            $locales = [];

            foreach ($livewire->getTranslatableLocales() as $locale) {
                $locales[$locale] =  $locale;
            }

            return $locales;
        });
    }
}