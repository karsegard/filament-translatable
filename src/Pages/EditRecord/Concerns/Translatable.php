<?php

namespace KDA\Filament\Translatable\Pages\EditRecord\Concerns;

use KDA\Filament\Translatable\Pages\Concerns\HasActiveLocaleSwitcher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

trait Translatable
{
    use HasActiveLocaleSwitcher;

    public $activeLocale = null;

    protected function fillForm(): void
    {
        $this->callHook('beforeFill');
       if ($this->activeLocale === null) {
            $this->setActiveLocale();
        }
        
   
  //      $data = $this->record->attributesToArray();
/*
        foreach (static::getResource()::getTranslatableAttributes() as $attribute) {
            $data[$attribute] = $this->record->getTranslation($attribute, $this->activeFormLocale);
        }

        $data = $this->mutateFormDataBeforeFill($data);

        $this->form->fill($data);
*/
        $this->record->setLocale($this->activeLocale);

        $data = $this->record->refresh()->attributesToArray();
     //   dump($this->record->getLocale(),$this->record->attributesToArray());

       // dump($this->activeLocale,$this->data);
        $this->form->fill($data);
        $this->callHook('afterFill');
    }

    protected function setActiveLocale(): void
    {
      $resource = static::getResource();

        /* $availableLocales = array_keys($this->record->getTranslations($resource::getTranslatableAttributes()[0]));
        $resourceLocales = $this->getTranslatableLocales();
        $defaultLocale = $resource::getDefaultTranslatableLocale();

        $this->activeLocale = $this->activeFormLocale = in_array($defaultLocale, $availableLocales) ? $defaultLocale : array_intersect($availableLocales, $resourceLocales)[0] ?? $defaultLocale;*/
      //  $this->record->setLocale($this->activeFormLocale);
      $this->activeLocale = app()->getLocale();
    }

    protected function handleRecordUpdate(Model $record, array $data): Model
    {
       // $record->fill(Arr::except($data, $record->getTranslatableAttributes()));
       $record->setLocale($this->activeLocale);
       $record->fill ($data);

    /*    foreach (Arr::only($data, $record->getTranslatableAttributes()) as $key => $value) {
            $record->setTranslation($key, $this->activeLocale, $value);
        }
*/
        $record->save();

        return $record;
    }

    public function updatedActiveLocale(): void
    {

        $this->fillForm();
    }

    public function updatingActiveLocale(): void
    {

      //  $this->save(shouldRedirect: false);
    }

    protected function getActions(): array
    {
        return array_merge(
            [$this->getActiveLocaleSelectAction()],
            parent::getActions(),
        );
    }
}
