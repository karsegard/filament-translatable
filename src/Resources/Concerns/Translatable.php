<?php

namespace KDA\Filament\Translatable\Resources\Concerns;

use Exception;
use KDA\Laravel\Translatable\Facades\Translatable as FacadesTranslatable;
use Spatie\Translatable\HasTranslations;

trait Translatable
{
    public static function getDefaultTranslatableLocale(): string
    {
        return app()->getLocale();
    }

    public static function getTranslatableAttributes(): array
    {
        $model = static::getModel();

        if (! method_exists($model, 'getTranslatableAttributes')) {
            throw new Exception("Model [{$model}] must use trait [" . HasTranslations::class . '].');
        }

        $attributes = app($model)->getTranslatableAttributes();

        if (! count($attributes)) {
            throw new Exception("Model [{$model}] must have [\$translatable] properties defined.");
        }

        return $attributes;
    }
    
    public static function getTranslatableLocales(): array
    {
        return FacadesTranslatable::getTranslatableLocales();
    }
}