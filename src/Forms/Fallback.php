<?php

namespace KDA\Filament\Translatable\Forms;
use Awcodes\Shout\Shout;


class Fallback extends Shout{

    public function setUp(): void
    {
        parent::setUp();

        $this->visible(fn ($record) => $record?->hasTranslationFallback());
        $this->content("Cet enregistrement n'est pas traduit, la langue par défaut est affichée afin de faciliter la traduction");
        $this->type('warning');
        $this->columnSpanFull();
    }
}

