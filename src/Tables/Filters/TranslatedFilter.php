<?php

namespace KDA\Filament\Translatable\Tables\Filters;
use Filament\Tables\Filters\TernaryFilter;
use Illuminate\Database\Eloquent\Builder;



class TranslatedFilter extends TernaryFilter
{
    public static function getDefaultName(): ?string
    {
        return 'translations';
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->placeholder(__('filament-translatable::filters.both'))
        ->trueLabel(__('filament-translatable::filters.translated'))
        ->falseLabel(__('filament-translatable::filters.not_translated'))
        ->queries(
            true: fn (Builder $query) => $query->fullyTranslated(),
            false: fn (Builder $query) => $query->partialyTranslated(),
            blank: fn (Builder $query) => $query,
        );
    }
}